from django.shortcuts import render

def intro(request):
    return render(request, 'index.html')

def my_writing(request):
    return render(request, 'my-writing.html')

def my_experiences(request):
    return render(request, 'my-experiences.html')

def my_resume_contact(request):
    return render(request, 'my-resume-contact.html')    
