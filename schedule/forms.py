from .models import Schedules, DAY_OF_WEEK
from django import forms
import datetime

class SchedulesForm(forms.Form):

    activity_name = forms.CharField(label = 'Your activity name', required = True, max_length=28, widget=forms.TextInput(
    	attrs={
    		'class': 'form-control',
    	}
    ))

    day = forms.ChoiceField(required = True, choices=DAY_OF_WEEK, widget=forms.Select(
    	attrs={
    		'class': 'form-control',
    	}
    ))

    date = forms.DateField(required = True, widget=forms.DateInput(
    	attrs={
    		'class': 'form-control',
    		'type' : 'date',
    	}
    ))

    start_activity_time = forms.TimeField(required = True, widget=forms.TimeInput(
    	attrs={
    		'class': 'form-control',
    		'type' : 'time',
    	}
    ))

    end_activity_time = forms.TimeField(required = True, widget=forms.TimeInput(
    	attrs={
    		'class': 'form-control',
    		'type' : 'time',
    	}
    ))

    location = forms.CharField(required = True, label = 'Location', max_length=28, widget=forms.TextInput(
    	attrs={
    		'class': 'form-control',
    	}
    ))

    category = forms.CharField(required = True, label = 'Category', max_length=28, widget=forms.TextInput(
    	attrs={
    		'class': 'form-control',
    	}
    ))

    class Meta:
        model = Schedules
        fields = '__all__'

    def clean_end_activity_time(self):
        start_activity_time = self.cleaned_data['start_activity_time']
        end_activity_time = self.cleaned_data['end_activity_time']
        day = self.cleaned_data['day']
        try:
            date = self.cleaned_data['date']
        except :
            return start_activity_time
       
        date_time_start = ('%s %s' % (date,start_activity_time))
        date_time_start = datetime.datetime.strptime(date_time_start, '%Y-%m-%d %H:%M:%S')
        date_day = date_time_start.strftime("%A")
        
        if date_time_start < datetime.datetime.now():
            self.add_error('start_activity_time', 'Cannot add past activity')
        if day != date_day:
            self.add_error('day', "Day mismatched with date")
        if end_activity_time < start_activity_time:
            self.add_error('end_activity_time', "End time should be greater than start time.")
        return start_activity_time




