# Generated by Django 2.2.6 on 2019-10-07 22:50

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20191008_0539'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedules',
            name='date',
            field=models.DateField(verbose_name=datetime.datetime(2019, 10, 7, 22, 50, 48, 40755, tzinfo=utc)),
        ),
    ]
