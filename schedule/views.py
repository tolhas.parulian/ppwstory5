from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import SchedulesForm
from .models import Schedules
from django.views.decorators.csrf import csrf_protect

response = {}

def index(request):
	schedules = Schedules.objects.all().values().order_by('date', 'start_activity_time')
	response['schedules'] = schedules
	response['SchedulesForm'] = SchedulesForm
	response['isFilled'] = ""
	return render(request, 'schedule.html', response)

@csrf_protect
def createSchedule(request):
    # Create a new Schedule
	form = SchedulesForm(request.POST or None)
	schedules = Schedules.objects.all().values().order_by('date', 'start_activity_time')
	response['schedules'] = schedules
	if request.method == 'POST' and form.is_valid():
		response['activity_name'] = request.POST['activity_name']
		response['day'] = request.POST['day']
		response['date'] = request.POST['date']
		response['start_activity_time'] = request.POST['start_activity_time']
		response['end_activity_time'] = request.POST['end_activity_time']
		response['location'] = request.POST['location']
		response['category'] = request.POST['category']
		schedule = Schedules(activity_name=response['activity_name'], day=response['day'], date=response['date'], 
			start_activity_time=response['start_activity_time'], end_activity_time=response['end_activity_time'], location=response['location'],
			category=response['category'])
		schedule.save()
		return HttpResponseRedirect("/schedules")
	response['SchedulesForm'] = form
	response['isFilled'] = request.POST['activity_name']
	return render(request, "schedule.html", response)

# def addschedule(request):
# 	response['SchedulesForm'] = SchedulesForm
# 	return render(request, 'addschedule.html', response)

def deleteschedule(request, schedule_id):
    Schedules.objects.filter(pk=schedule_id).delete()
    return HttpResponseRedirect('/schedules',)

def resetschedule(request):
    all_schedules = Schedules.objects.all()
    for data in all_schedules:
        data.delete()
    return HttpResponseRedirect('/schedules',)


