from django.urls import path
from . import views

app_name = "schedule"

urlpatterns = [
    path('', views.index, name = 'all_schedules'),
    path('createschedule', views.createSchedule, name = 'create_schedule'),
    path('resetschedule', views.resetschedule, name = 'reset_schedule'),
    path('<int:schedule_id>/', views.deleteschedule, name = 'delete_schedule'),


]