from django.db import models
from datetime import datetime, date
from django.utils import timezone

# reference: https://gitlab.com/ahmad_fauzan458/story
DAY_OF_WEEK = [
	("Sunday", "Sunday"),
	("Monday", "Monday"),
	("Tuesday", "Tuesday"),
	("Wednesday", "Wednesday"),
	("Thursday", "Thursday"),
	("Friday", "Friday"),
	("Saturday", "Saturday"),
]	

class Schedules(models.Model):
	activity_name = models.CharField(max_length=28)
	day = models.CharField(choices=DAY_OF_WEEK, max_length=9)
	date = models.DateField()
	start_activity_time = models.TimeField()
	end_activity_time = models.TimeField()
	location = models.CharField(max_length=28) 
	category = models.CharField(max_length=11)
	def __str__(self):
	 return self.activity_name + self.start_activity_time.strftime("%I")


